# tdlf_jam_theory

Le français suit // Español abajo

## About this repo

The definition found here is the theory underpinnings, observations and definitions of the jam arrived at by the combined efforts of members of the Percepto collective, the Laboratoire d'art et de recherches décoloniale de l'UQAM (LabARD) and the SBC galerie d'art contemporain and multiple collaborators. It is placed here in the hopes it can be useful and receive other contributions.

## Authors and acknowledgment

Amanda Gutiérrez, Alexandre Castonguay, Philippe-Aubert Gauthier, Carla Rangel, Manuel Chantre, Yesica Duarte, Nuria Cartón de Grammont, Antoine Bertron, Danny Perreault, Maria Paula Lonegro,  Patrick Coulombe

## Citation

TDLF. (Technologies de la fête). TDLF Jam Theory. GitLab. 2024, https://gitlab.com/tdlf/tdlf_jam_theory

## Contributing
We are very much open to collaborate on these definitions and exchanging about them.  Feel free to contact us through email or the gitlab repo contact.

## License
This project is distributed under the LGPL. We believe that this license enables to recognize the collective work of the participants and allows new contributions.

## Definition of the jam

* Intro : The following was arrived at through numerous discussions, organising an exhibition, and putting into practice the jam while co-developing electronic tools for the jam, it's theory and practice.

* The jam has a history and a general understanding inherited from musical practices, jazz musicians for instance. It is important to note that our understanding of the jam is situated away from notions of proficiency or pre-learned musical structures, the jam is understood as a wide electro-acoustic audio visual spectrum, from electronic music to noise. 

- Horizontality : the jam is immanent and the struture and tools emerges from all participants
- Develops active listening and a sense of when to intervene
- Communality and support : through shared experiences, the jam fosters a sense of belonging, of community
- It exists away from language and concepts
- Inclusive : open to all capacities, made to be shared
- Participants have the right to error : there is no 'right note' and new proposals become material for others
- Open to glitches : see above
- Close to orality : in that it is circular, it's structures are recurrent and additive or substractive over time.
- Improvisational :  away from questions of pre-determined research outcomes. Needs to be expanded as there are structures and sonorities common to the style of electronic music. In another sense, noise based or other types of jam might beless structured. 
- Openness to others : if you hear yourself in the mix, lower your volume
- Incorporates the joy of doing into research : recherche-récréation
- No proficiency expected : it's a shared experience through the introduction of tools, both electronic and discursive.
- Co-designed : everyone gets a say

## Interesting readings 

- Party studies, Brandon Labelle et al
- Raving, McKenzie Wark
- Historia Univeral Del After
- Dance Usted

----------------------------------

(FR)

## À propos de ce dépôt

Les définitions trouvées ici sont les bases théoriques, observations et définitions du jam, résultant des efforts combinés des membres du collectif Percepto, du Laboratoire d'art et de recherches décoloniales de l'UQAM (LabARD) et de la SBC galerie d'art contemporain ainsi que de multiples collaborateurs. Il est placé ici dans l'espoir qu'il puisse être utile et recevoir d'autres contributions.

## Auteurs

Amanda Gutiérrez, Alexandre Castonguay, Philippe-Aubert Gauthier, Carla Rangel, Manuel Chantre, Yesica Duarte, Nuria Cartón de Grammont, Antoine Bertron, Danny Perreault, Maria Paula Lonegro, Patrick Coulombe

## Définition du jam

* Intro : Ce qui suit a été obtenu par de nombreuses discussions, l'organisation d'une exposition et la mise en pratique du jam tout en co-développant des outils électroniques pour le jam, sa théorie et sa pratique.

* Le 'jam' a une connotation héritée de pratiques musicales telles que le free Jazz.  Il est important de clarifier que notre entendement du jam est éloigné des notions de compétence ou de structures musicales pré-apprises, le jam est compris comme un large spectre audio-visuel électro-acoustique, de la musique électronique au bruit.

- Horizontalité : le jam est immanent et la structure et les outils émergent de tous les participants
- Développe l'écoute active : nécessaire pour développer le sens du moment où intervenir
- Communauté et soutien : à travers des expériences partagées, le jam favorise un sentiment d'appartenance, de communauté
- Il existe loin du langage et des concepts
- Inclusif : ouvert à toutes les capacités, fait pour être partagé
- Les participants ont le droit à l'erreur : il n'y a pas de 'bonne note', l'erreur est matière pour les autres participants
- Ouvert au glitch : voir (ci-haut)
- Proche de l'oralité : il est circulaire, ses structures sont récurrentes et additives ou soustractives au fil du temps.
- Improvisation : loin des questions de résultats de recherche prédéterminés.  Ceci dit, la musique électronique possède  ses règles et structures inhérentes qui peuvent être assimilés par les participants. Par contre un jam moins strucutré, de noise par exemple, ou encore dans une autre discipline (comme la discipline curatotiale par exemple) pourrait être plus libre ou du moins avoir des structures différentes.
- Ouverture aux autres
- Intègre la joie de faire dans la recherche : recherche-récréation
- Pas de compétence attendue : c'est une expérience partagée à travers l'introduction d'outils, à la fois électroniques et discursifs.
- Co-conçu : tout le monde a son mot à dire

## Références 

- Party Studies, Brandon Labelle et al.
- Raving, McKenzie Wark
- Historia Univeral Del After
- Dance Usted

-----------------------------------

(ES)

## Sobre este repositorio

Las definiciones que se encuentran aquí son los fundamentos teóricos, observaciones y definiciones del jam, logrados por los esfuerzos combinados de los miembros del colectivo Percepto, el Laboratorio de arte e investigaciones decoloniales de la UQAM (LabARD) y la SBC galería de arte contemporáneo y múltiples colaboradores. Se coloca aquí con la esperanza de que pueda ser útil y recibir otras contribuciones.

## Autores

Amanda Gutiérrez, Alexandre Castonguay, Philippe-Aubert Gauthier, Carla Rangel, Manuel Chantre, Yesica Duarte, Nuria Cartón de Grammont, Antoine Bertron, Danny Perreault, Maria Paula Lonegro, Patrick Coulombe

## Definición del jam

* Intro: Lo siguiente se logró a través de numerosas discusiones, la organización de una exposición y la puesta en práctica del jam mientras se co-desarrollaban herramientas electrónicas para el jam, su teoría y práctica.

* Alejándose de las nociones de competencia o estructuras musicales pre-aprendidas, el jam se entiende como un amplio espectro audiovisual electro-acústico, desde la música electrónica hasta el ruido.

- Horizontalidad : el jam es inmanente y la estructura y herramientas emergen de todos los participantes
- Desarrolla la escucha activa : es necesaria para tener un sentido de cuándo intervenir
- Comunidad y apoyo : a través de experiencias compartidas, el jam fomenta un sentido de pertenencia, de comunidad
- Existe lejos del lenguaje y los conceptos
- Inclusivo : abierto a todas las capacidades, hecho para ser compartido
- Los participantes tienen derecho al error : no hay 'nota correcta'
- Abierto al 'glitch'
- Cercano a la oralidad : en el sentido de que es circular, sus estructuras son recurrentes y aditivas o sustractivas a lo largo del tiempo.
- Improvisación : alejado de las cuestiones de resultados de investigación predeterminados, o lo más que posible
- Apertura a los demás
- Incorpora la alegría de hacer en la investigación : investigación-recreación
- No se espera competencia : es una experiencia compartida a través de la introducción de herramientas, tanto electrónicas como discursivas.
- Co-diseñado : todos tienen voz

## Referencias

- Party Studies, Brandon Labelle et al.
- Raving, McKenzie Wark
- Historia Univeral Del After
- Dance Usted



***





